const Router = require('koa-router'),
  router = new Router(),
  model = require('./rhinoceros');

router.get('/rhinoceros', (ctx, next) => {
  let rhinoceroses
  if (ctx.request.query.name || ctx.request.query.species) {
    rhinoceroses = model.getAllWithFilter(ctx.request.query)
  } else {
    rhinoceroses = model.getAll();
  }
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros/id/:id', (ctx, next) => {
  const rhinoceros = model.getRhinocerosId(ctx.params.id);
  ctx.response.body = rhinoceros
})

router.get('/rhinoceros/endangered', (ctx, next) => {
  const endangeredRhinos = model.getEndangeredRhinos();
  ctx.response.body = endangeredRhinos
})

router.post('/rhinoceros', (ctx, next) => {
  try {
    ctx.response.body = model.newRhinoceros(ctx.request.body);
  } catch (err) {
    console.log(err)
    ctx.response.body = err
    ctx.status = 400
  }
});

module.exports = router;
