const uuidv4 = require('uuid/v4');
let rhinoceroses = require('./data');

const allowedSpecies = [
  "white_rhinoceros",
  "black_rhinoceros",
  "indian_rhinoceros",
  "javan_rhinoceros",
  "sumatran_rhinoceros", // Not sure what y'alls' opinions are on trailing commas
]

const speciesCount = allowedSpecies.reduce((acc ,curr) => (acc[curr] = 0, acc), {})
rhinoceroses.forEach(rhino => {
  speciesCount[rhino.species]++
})

exports.getAll = () => {
  return rhinoceroses;
};

exports.getAllWithFilter = queries => {
  let foundRhinoceroses
  if (queries.name && queries.species) {
    foundRhinoceroses = rhinoceroses.filter((rhino => {
      return rhino.name == queries.name && rhino.species == queries.species;
    }))
  } else if (queries.name) {
    foundRhinoceroses = rhinoceroses.filter((rhino => {
      return rhino.name == queries.name
    }))
  } else if (queries.species) {
    foundRhinoceroses = rhinoceroses.filter((rhino) => {
      return rhino.species == queries.species
    })
  }

  return foundRhinoceroses
}

exports.getRhinocerosId = id => {
  const rhinoceros = rhinoceroses.find(rhino => {
    return rhino.id == id;
  })
  return rhinoceros
}

exports.getEndangeredRhinos = () => {
  endangeredRhinos = rhinoceroses.filter(rhino => {
    return speciesCount[rhino.species] <= 2
  })
  return endangeredRhinos
}
exports.newRhinoceros = data => {
  let errorMessage = ""
  if (data.name.length > 20 ||data.name.length < 1) {
    errorMessage += '`name` is a required field, and must be between 1 and 20 characters\n'
  }
  if (!allowedSpecies.find(species => { species == data.species })) {
    errorMessage += `\`species\` is a required field and must be one of ${allowedSpecies}`
  }
  if (errorMessage) throw errorMessage 

  const newRhino = {
    id: uuidv4(),
    name: data.name,
    species: data.species
  };
  rhinoceroses.push(newRhino);
  speciesCount[data.species]++
  return newRhino;
};
